package com.example.faccigemazambranoconvertidor_libras;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

public class pantalla2 extends AppCompatActivity {
    TextView textViewR;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pantalla2);

        textViewR = (TextView)findViewById(R.id.textViewresultadoconversion);

        Bundle bundle = this.getIntent().getExtras();
        textViewR.setText(bundle.getString("dato"));
        Toast.makeText(getApplicationContext(),"Conversion Finaliza", Toast.LENGTH_SHORT).show();
    }
}
