package com.example.faccigemazambranoconvertidor_libras;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    Button button;
    EditText EditTextingreso;
    TextView textViewR;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button = (Button) findViewById(R.id.buttonCF);
        EditTextingreso = (EditText) findViewById(R.id.editTextM);
        textViewR = (TextView) findViewById(R.id.textViewresultadoconversion);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, pantalla2.class);
                Bundle bundle = new Bundle();
                bundle.putString("dato", textViewR.getText().toString());
                float valor = Float.parseFloat(EditTextingreso.getText().toString());
                float resultadoC = (float) ((valor * 453.592));
                textViewR.setText(String.valueOf(resultadoC+ " gramos "));
                bundle.putString("dato", textViewR.getText().toString());
                intent.putExtras(bundle);
                startActivity(intent);

            }
        });

    }
}
